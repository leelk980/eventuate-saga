package com.example.shared.saga.event

import io.eventuate.tram.events.common.DomainEvent

data class OrderCreatedEvent(
    val name: String = "order-created",
    val id: Long,
) : DomainEvent