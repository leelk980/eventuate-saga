package com.example.shared.saga.command

import io.eventuate.tram.commands.common.Command

data class PayOrderRollbackCommand(
    val orderId: Long,
    val price: Long,
) : Command {
    data class Response(
        val id: String,
        val success: Boolean,
    )
}
