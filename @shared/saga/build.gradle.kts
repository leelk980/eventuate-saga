dependencies {
    // Bom
    api(platform("io.eventuate.platform:eventuate-platform-dependencies:2022.0.RELEASE"))
    // Tram
    api("io.eventuate.tram.core:eventuate-tram-spring-jdbc-kafka")
    api("io.eventuate.tram.core:eventuate-tram-spring-commands")
    api("io.eventuate.tram.core:eventuate-tram-spring-commands-starter")
    api("io.eventuate.tram.core:eventuate-tram-spring-events")
    api("io.eventuate.tram.core:eventuate-tram-spring-events-starter")
    // Saga
    api("io.eventuate.tram.sagas:eventuate-tram-sagas-spring-orchestration-simple-dsl")
    api("io.eventuate.tram.sagas:eventuate-tram-sagas-spring-orchestration-simple-dsl-starter")
    api("io.eventuate.tram.sagas:eventuate-tram-sagas-spring-participant")
    api("io.eventuate.tram.sagas:eventuate-tram-sagas-spring-participant-starter")

    testApi("io.eventuate.tram.core:eventuate-tram-spring-in-memory")
    testApi("io.eventuate.tram.sagas:eventuate-tram-sagas-spring-testing-support")
}
