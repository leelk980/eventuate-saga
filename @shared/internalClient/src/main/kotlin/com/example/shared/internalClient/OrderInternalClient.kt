package com.example.shared.internalClient

import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient

class OrderInternalClient {
    private val webClient = WebClient
        .builder()
        .baseUrl("http://localhost:8080/api/v2")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .build()

    fun findAllOrdersByUserId(userId: Long): String? {
        return webClient
            .get()
            .uri("/orders/users/$userId")
            .retrieve()
            .bodyToMono(String::class.java)
            .block()
    }
}
