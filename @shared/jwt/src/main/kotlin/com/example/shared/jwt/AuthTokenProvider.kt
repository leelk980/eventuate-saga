package com.example.shared.jwt

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*

open class AuthTokenProvider {
    private val expires = 10 * 60 * 1000
    private val secret = "secret"
    private val objectMapper: ObjectMapper = jacksonObjectMapper()

    init {
        objectMapper.registerModule(JavaTimeModule())
        objectMapper.registerKotlinModule()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    data class AccessTokenPayload(
        val userId: Long,
        val roles: List<String>,
    )

    fun generatePayload(userId: Long, roles: List<String>) = AccessTokenPayload(userId, roles)

    fun encodeAccessToken(payload: AccessTokenPayload): String {
        val claims = Jwts.claims(
            objectMapper.convertValue<Map<String, Any>>(payload)
        )

        return Jwts.builder()
            .setSubject("auth_token")
            .setClaims(claims)
            .setExpiration(Date(System.currentTimeMillis() + expires))
            .signWith(SignatureAlgorithm.HS256, secret)
            .compact()
    }

    fun decodeAccessToken(token: String): AccessTokenPayload {
        val claims = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)

        return objectMapper.convertValue(claims.body)
    }
}