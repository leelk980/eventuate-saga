package com.example.shared.security.config

import com.example.shared.security.component.AccessDeniedHandler
import com.example.shared.security.component.AuthenticationEntryPoint
import com.example.shared.security.component.AuthenticationFilter
import com.example.shared.security.component.RoleValidator
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
abstract class BaseSecurityConfig : WebSecurityConfigurerAdapter() {
    @Bean
    fun roleValidator(): RoleValidator {
        return RoleValidator()
    }

    override fun configure(web: WebSecurity) {
        web.ignoring()
            .antMatchers("/favicon.ico")
    }

    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(AuthenticationEntryPoint())
            .accessDeniedHandler(AccessDeniedHandler())
            .and()
            .authorizeRequests()
            .anyRequest().permitAll()
            .and()
            .addFilterBefore(AuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }
}