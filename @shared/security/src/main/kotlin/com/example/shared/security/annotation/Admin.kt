package com.example.shared.security.annotation

import org.springframework.security.access.prepost.PreAuthorize

@PreAuthorize("@roleValidator.isAdmin()")
annotation class Admin
