package com.example.shared.security.component

import com.example.shared.jwt.AuthTokenProvider
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

/** @description
 * - 모든 요청이 거치는 filter
 * - 토큰이 없으면 그냥 pass, 있으면 decode한 user 정보를 SecurityContextHolder에 넣고 pass
 * */
class AuthenticationFilter : GenericFilterBean() {
    private val authTokenProvider: AuthTokenProvider = AuthTokenProvider()

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
        val httpServletRequest = servletRequest as HttpServletRequest

        val authHeader = httpServletRequest.getHeader("authorization")
            ?: return filterChain.doFilter(servletRequest, servletResponse)

        val accessToken = authHeader.split("Bearer ").getOrNull(1)
            ?: return filterChain.doFilter(servletRequest, servletResponse)

        val decoded = authTokenProvider.decodeAccessToken(accessToken)

        SecurityContextHolder.getContext().authentication = AuthenticationToken(
            id = decoded.userId,
            roles = decoded.roles,
            accessToken = accessToken,
        )

        return filterChain.doFilter(servletRequest, servletResponse)
    }
}