package com.example.shared.security.annotation

enum class SecurityRoleEnum {
    ADMIN,
    GENERAL1,
    GENERAL2;
}