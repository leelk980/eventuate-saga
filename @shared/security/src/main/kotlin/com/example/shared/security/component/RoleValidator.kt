package com.example.shared.security.component

import com.example.shared.security.annotation.SecurityRoleEnum
import org.springframework.security.core.context.SecurityContextHolder


/** @description
 * - role 기반의 authorization handler
 * */
class RoleValidator {
    fun isAdmin() = SecurityContextHolder.getContext()
        ?.authentication
        ?.authorities
        ?.any { it.authority == SecurityRoleEnum.ADMIN.name } ?: false

    fun isGeneral() = SecurityContextHolder.getContext()
        ?.authentication
        ?.authorities
        ?.any { listOf(SecurityRoleEnum.GENERAL1.name, SecurityRoleEnum.GENERAL2.name).contains(it.authority) } ?: false
}
