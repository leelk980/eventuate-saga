package com.example.shared.security.component

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class AuthenticationToken(
    private val id: Long,
    private val roles: List<String>,
    private val accessToken: String,
) : AbstractAuthenticationToken(null) {
    init {
        isAuthenticated = true
    }

    override fun getAuthorities(): List<GrantedAuthority> {
        return roles.map { GrantedAuthority { it } }
    }

    override fun getName() = "currentUser"

    override fun getPrincipal() = null

    override fun getDetails() = lazy { CurrUser(id, roles) }.value

    override fun getCredentials() = accessToken
}

data class CurrUser(
    val id: Long,
    val roles: List<String>
)

