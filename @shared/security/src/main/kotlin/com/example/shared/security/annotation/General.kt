package com.example.shared.security.annotation

import org.springframework.security.access.prepost.PreAuthorize

@PreAuthorize("@roleValidator.isGeneral()")
annotation class General