package com.example.shared.security.annotation

import org.springframework.security.access.prepost.PreAuthorize

@PreAuthorize("permitAll()")
annotation class Public