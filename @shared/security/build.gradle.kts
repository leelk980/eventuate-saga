dependencies {
    implementation(project(":@shared:jwt"))

    api("org.springframework.boot:spring-boot-starter-security")
    
    testApi("org.springframework.security:spring-security-test")
}
