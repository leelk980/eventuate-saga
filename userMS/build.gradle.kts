dependencies {
    implementation(project(":@shared:saga"))
    implementation(project(":@shared:security"))
    implementation(project(":@shared:jwt"))
    implementation(project(":@shared:internalClient"))
}
