package com.example.userMS.`interface`.external.view

import com.example.userMS.common.data.Pagination

data class FindUserPageView(
    val content: List<User>,
    val pagination: Pagination,
) {
    data class User(
        val id: Long,
        val name: String,
    )
}
