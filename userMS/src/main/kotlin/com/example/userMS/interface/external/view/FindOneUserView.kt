package com.example.userMS.`interface`.external.view

import com.example.userMS.common.enumerate.UserRoleTitleEnum

data class FindOneUserView(
    val id: Long = 0,
    val name: String,
    val userRoles: List<UserRoleTitleEnum>,
    val orders: String?
)
