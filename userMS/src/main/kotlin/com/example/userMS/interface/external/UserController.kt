package com.example.userMS.`interface`.external

import com.example.shared.security.annotation.Admin
import com.example.shared.security.annotation.Authenticate
import com.example.shared.security.annotation.General
import com.example.shared.security.annotation.Public
import com.example.userMS.application.FacadeService
import com.example.userMS.application.SagaService
import com.example.userMS.`interface`.external.req.FindUserPageQuery
import com.example.userMS.`interface`.external.req.SignInBody
import com.example.userMS.`interface`.external.req.SignUpBody
import com.example.userMS.`interface`.external.view.FindOneUserView
import com.example.userMS.`interface`.external.view.FindUserPageView
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1")
class UserController(
    private val sagaService: SagaService,
    private val facadeService: FacadeService
) {
    @Public
    @PostMapping("/sign-up")
    fun signUp(@RequestBody body: SignUpBody): Long {
        return sagaService.runSignUpSaga(body)
    }

    @Public
    @PostMapping("/sign-in")
    fun signIn(@RequestBody body: SignInBody): String {
        return sagaService.runSignInSaga(body)
    }

    @Public
    @GetMapping("/users")
    fun findUserPage(@ModelAttribute query: FindUserPageQuery): FindUserPageView {
        return facadeService.findUserPage(query)
    }

    @Public
    @GetMapping("/users/{id}")
    fun findOneUserById(@PathVariable id: Long): FindOneUserView {
        return facadeService.findOneUserById(id)
    }

    @Authenticate
    @PostMapping("/me")
    fun getMe(): Authentication {
        return SecurityContextHolder.getContext().authentication
    }

    @Authenticate
    @PostMapping("/auth")
    fun protected(): String {
        return "only-auth"
    }

    @Admin
    @PostMapping("/admin")
    fun admin(): String {
        return "only-admin"
    }

    @General
    @PostMapping("/general")
    fun test(): Authentication {
        return SecurityContextHolder.getContext().authentication
    }
}
