package com.example.userMS.`interface`.external.req

data class SignUpBody(
    val name: String,
    val password: String
)
