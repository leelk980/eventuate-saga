package com.example.userMS.`interface`.external.req

data class SignInBody(
    val name: String,
    val password: String
)
