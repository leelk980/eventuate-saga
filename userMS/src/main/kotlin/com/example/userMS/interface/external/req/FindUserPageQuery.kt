package com.example.userMS.`interface`.external.req

import com.example.userMS.common.enumerate.UserRoleTitleEnum

data class FindUserPageQuery(
    val name: String?,
    val userRoles: List<UserRoleTitleEnum>,
    val targetPage: Int,
    val limit: Int
)
