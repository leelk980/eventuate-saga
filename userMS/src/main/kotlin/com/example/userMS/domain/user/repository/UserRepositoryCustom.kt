package com.example.userMS.domain.user.repository

import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.entity.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface UserRepositoryCustom {
    fun findPage(name: String?, userRoles: List<UserRoleTitleEnum>, pageable: Pageable): Page<User>
}