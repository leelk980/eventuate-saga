package com.example.userMS.domain.user.dto

import com.example.userMS.common.enumerate.UserRoleTitleEnum

data class FindOneByIdDto(
    val id: Long,
    val name: String,
    val userRoles: List<UserRoleTitleEnum>
)
