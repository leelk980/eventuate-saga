package com.example.userMS.domain.user.entity

import com.example.userMS.common.enumerate.UserRoleTitleEnum
import javax.persistence.*

@Entity
@Table(name = "user_role")
open class UserRole(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0,

    @Column(name = "title", nullable = false)
    @Enumerated(EnumType.STRING)
    open var title: UserRoleTitleEnum,

    @Column(name = "user_id", nullable = false)
    open var userId: Long,
) : BaseEntity() {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    open lateinit var user: User
}