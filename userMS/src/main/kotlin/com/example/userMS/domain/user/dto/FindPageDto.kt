package com.example.userMS.domain.user.dto

import com.example.userMS.common.data.Pagination

data class FindPageDto(
    val content: List<User>,
    val pagination: Pagination,
) {
    data class User(
        val id: Long,
        val name: String,
    )
}
