package com.example.userMS.domain.user.entity

import javax.persistence.*

@Entity
@Table(name = "user")
open class User(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0,

    @Column(name = "name", nullable = false)
    open var name: String,

    @Column(name = "password", nullable = true)
    open var password: String,
) : BaseEntity() {
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    open lateinit var userRoles: MutableList<UserRole>
}