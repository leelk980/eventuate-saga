package com.example.userMS.domain.user

import com.example.shared.jwt.AuthTokenProvider
import com.example.shared.security.annotation.SecurityRoleEnum
import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.entity.User
import com.example.userMS.domain.user.entity.UserRole
import com.example.userMS.domain.user.repository.UserRepository
import com.example.userMS.domain.user.repository.UserRoleRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class UserCommandService(
    private val userRepository: UserRepository,
    private val userRoleRepository: UserRoleRepository,
    private val bCryptPasswordEncoder: BCryptPasswordEncoder,
    private val authTokenProvider: AuthTokenProvider
) {
    fun createAggregate(
        name: String,
        password: String
    ): Long {
        val newUser = userRepository.save(
            User(
                name = name,
                password = bCryptPasswordEncoder.encode(password)
            )
        )

        userRoleRepository.saveAll(
            listOf(
                UserRole(
                    title = UserRoleTitleEnum.GENERAL,
                    userId = newUser.id
                )
            )
        )

        return newUser.id
    }

    fun deleteAggregateById(id: Long): Long {
        userRepository.deleteById(id)
        return id
    }

    fun login(name: String, password: String): String {
        val user = userRepository.findOneByName(name)

        if (user == null || !bCryptPasswordEncoder.matches(password, user.password)) {
            throw Exception("invalid name or password")
        }

        return authTokenProvider.encodeAccessToken(
            authTokenProvider.generatePayload(
                userId = user.id,
                roles = user.userRoles.map {
                    when (it.title) {
                        UserRoleTitleEnum.ADMIN -> SecurityRoleEnum.ADMIN
                        UserRoleTitleEnum.MEMBERSHIP -> SecurityRoleEnum.GENERAL1
                        UserRoleTitleEnum.GENERAL -> SecurityRoleEnum.GENERAL2
                    }.name
                }
            )
        )
    }
}
