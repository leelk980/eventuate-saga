package com.example.userMS.domain.user

import com.example.userMS.common.data.Pagination
import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.dto.FindOneByIdDto
import com.example.userMS.domain.user.dto.FindPageDto
import com.example.userMS.domain.user.repository.UserRepository
import com.example.userMS.domain.user.repository.UserRoleRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class UserQueryService(
    private val userRepository: UserRepository,
    private val userRoleRepository: UserRoleRepository
) {
    fun findOneById(id: Long): FindOneByIdDto {
        val userEntity = userRepository.findByIdOrNull(id) ?: throw Exception("user not found")

        return FindOneByIdDto(
            id = userEntity.id,
            name = userEntity.name,
            userRoles = userEntity.userRoles.map { it.title }
        )
    }

    fun findPage(name: String?, userRoles: List<UserRoleTitleEnum>, targetPage: Int, limit: Int): FindPageDto {
        val userPage = userRepository.findPage(
            name = name,
            userRoles = userRoles,
            pageable = PageRequest.of(
                targetPage,
                limit,
                Sort.by("id", "name").ascending()
            )
        )

        return with(userPage) {
            FindPageDto(
                content = content.map {
                    FindPageDto.User(
                        id = it.id,
                        name = it.name,
                    )
                },
                pagination = Pagination(
                    totalPages = totalPages,
                    totalElements = totalElements,
                    currentPage = number,
                    limit = size,
                )
            )
        }
    }
}
