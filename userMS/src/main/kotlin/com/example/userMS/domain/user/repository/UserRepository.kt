package com.example.userMS.domain.user.repository

import com.example.userMS.domain.user.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long>, UserRepositoryCustom {
    fun findOneByName(name: String): User?
}