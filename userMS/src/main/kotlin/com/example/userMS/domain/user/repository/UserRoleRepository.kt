package com.example.userMS.domain.user.repository

import com.example.userMS.domain.user.entity.UserRole
import org.springframework.data.jpa.repository.JpaRepository

interface UserRoleRepository : JpaRepository<UserRole, Long>