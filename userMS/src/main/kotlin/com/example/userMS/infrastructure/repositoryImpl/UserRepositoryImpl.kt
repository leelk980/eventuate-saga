package com.example.userMS.infrastructure.repositoryImpl

import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.entity.QUser
import com.example.userMS.domain.user.entity.QUserRole
import com.example.userMS.domain.user.entity.User
import com.example.userMS.domain.user.repository.UserRepositoryCustom
import com.querydsl.core.types.Order
import com.querydsl.core.types.OrderSpecifier
import com.querydsl.jpa.JPAExpressions
import com.querydsl.jpa.impl.JPAQueryFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository

@Repository
class UserRepositoryImpl(
    private val jpaQueryFactory: JPAQueryFactory,
) : UserRepositoryCustom {
    private val u = QUser.user
    private val ur = QUserRole.userRole

    override fun findPage(name: String?, userRoles: List<UserRoleTitleEnum>, pageable: Pageable): Page<User> {
        val query = jpaQueryFactory.from(u)

        if (name != null) {
            query.where(u.name.eq(name))
        }

        if (userRoles.isNotEmpty()) {
            query.where(
                JPAExpressions
                    .selectOne()
                    .from(ur)
                    .where(ur.userId.eq(u.id), ur.title.`in`(userRoles))
                    .exists()
            )
        }

        val totalCount = query
            .clone()
            .select(u.id.count())
            .fetchOne() ?: 0

        val limit = pageable.pageSize.toLong()
        val content = query
            .clone()
            .select(u)
            .offset(pageable.offset - limit)
            .limit(pageable.pageSize.toLong())
            .orderBy(*pageable.sort.map {
                val direction = if (it.isAscending) Order.ASC else Order.DESC
                val property = when (it.property) {
                    "id" -> u.id
                    "name" -> u.name
                    else -> u.id
                }
                OrderSpecifier(direction, property)
            }.toList().toTypedArray())
            .fetch()

        return PageImpl(content, pageable, totalCount)
    }
}