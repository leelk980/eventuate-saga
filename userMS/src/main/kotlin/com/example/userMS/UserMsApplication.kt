package com.example.userMS

import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.UserCommandService
import com.example.userMS.domain.user.entity.UserRole
import com.example.userMS.domain.user.repository.UserRoleRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.stereotype.Component

@SpringBootApplication
@EnableJpaAuditing
class UserMSApplication

fun main(args: Array<String>) {
    runApplication<UserMSApplication>(*args)
}

@Component
class CommandLineRunner(
    private val userCommandService: UserCommandService,
    private val userRoleRepository: UserRoleRepository
) : CommandLineRunner {
    override fun run(vararg args: String) {
        (1..30).forEach {
            userCommandService.createAggregate("testA", "1234")

        }
        (1..30).forEach {
            userCommandService.createAggregate("testB", "1234")

        }
        (1..30).forEach {
            userCommandService.createAggregate("testC", "1234")

        }
        (1..90 step 2).forEach {
            userRoleRepository.save(
                UserRole(title = UserRoleTitleEnum.ADMIN, userId = it.toLong())
            )
        }
    }
}
