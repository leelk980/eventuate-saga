package com.example.userMS.common.data

data class Pagination(
    val totalPages: Int,
    val totalElements: Long,
    val currentPage: Int,
    val limit: Int
)