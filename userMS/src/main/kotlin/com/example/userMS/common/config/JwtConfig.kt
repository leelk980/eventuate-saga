package com.example.userMS.common.config

import com.example.shared.jwt.AuthTokenProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JwtConfig {
    @Bean
    fun authTokenProvider() = AuthTokenProvider()
}