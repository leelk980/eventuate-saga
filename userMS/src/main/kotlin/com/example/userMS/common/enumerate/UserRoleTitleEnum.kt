package com.example.userMS.common.enumerate

enum class UserRoleTitleEnum {
    ADMIN,
    MEMBERSHIP,
    GENERAL,
}