package com.example.userMS.common.config

import com.example.shared.internalClient.OrderInternalClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class InternalClientConfig {
    @Bean
    fun orderInternalClient() = OrderInternalClient()
}
