package com.example.userMS.application.saga

import com.example.userMS.domain.user.UserCommandService
import io.eventuate.tram.sagas.orchestration.SagaDefinition
import io.eventuate.tram.sagas.simpledsl.SimpleSaga
import org.springframework.stereotype.Component

@Component
class SignInSaga(
    private val userCommandService: UserCommandService
) : SimpleSaga<SignInSaga.Data> {
    data class Data(
        val name: String,
        val password: String
    ) {
        var accessToken: String = ""
    }

    fun generateData(name: String, password: String) = Data(
        name = name,
        password = password
    )

    private val sagaDefinition = step()
        .invokeLocal(::login)
        .build()

    override fun getSagaDefinition(): SagaDefinition<Data> = sagaDefinition

    fun login(data: Data) {
        data.accessToken = userCommandService.login(data.name, data.password)
    }
}
