package com.example.userMS.application

import com.example.userMS.application.saga.SignInSaga
import com.example.userMS.application.saga.SignUpSaga
import com.example.userMS.`interface`.external.req.SignInBody
import com.example.userMS.`interface`.external.req.SignUpBody
import io.eventuate.tram.sagas.orchestration.SagaInstanceFactory
import org.springframework.stereotype.Service

@Service
class SagaService(
    private val sagaInstanceFactory: SagaInstanceFactory,
    private val signUpSaga: SignUpSaga,
    private val signInSaga: SignInSaga
) {
    fun runSignUpSaga(body: SignUpBody): Long {
        val data = signUpSaga.generateData(body.name, body.password)
        sagaInstanceFactory.create(signUpSaga, data)

        return data.id
    }

    fun runSignInSaga(body: SignInBody): String {
        val data = signInSaga.generateData(body.name, body.password)
        sagaInstanceFactory.create(signInSaga, data)

        return data.accessToken
    }
}
