package com.example.userMS.application

import com.example.shared.internalClient.OrderInternalClient
import com.example.userMS.domain.user.UserQueryService
import com.example.userMS.`interface`.external.req.FindUserPageQuery
import com.example.userMS.`interface`.external.view.FindOneUserView
import com.example.userMS.`interface`.external.view.FindUserPageView
import org.springframework.stereotype.Service

@Service
class FacadeService(
    private val userQueryService: UserQueryService,
    private val orderInternalClient: OrderInternalClient
) {
    fun findOneUserById(id: Long): FindOneUserView {
        val userDto = userQueryService.findOneById(id)
        val orders = orderInternalClient.findAllOrdersByUserId(id)

        return FindOneUserView(
            id = userDto.id,
            name = userDto.name,
            userRoles = userDto.userRoles,
            orders = orders
        )
    }

    fun findUserPage(query: FindUserPageQuery): FindUserPageView {
        val userPageDto = userQueryService.findPage(
            name = query.name,
            userRoles = query.userRoles,
            targetPage = query.targetPage,
            limit = query.limit
        )

        return FindUserPageView(
            content = userPageDto.content.map {
                FindUserPageView.User(
                    id = it.id,
                    name = it.name
                )
            },
            pagination = userPageDto.pagination
        )
    }
}
