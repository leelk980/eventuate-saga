package com.example.userMS.application.saga

import com.example.userMS.domain.user.UserCommandService
import io.eventuate.tram.sagas.orchestration.SagaDefinition
import io.eventuate.tram.sagas.simpledsl.SimpleSaga
import org.springframework.stereotype.Component

@Component
class SignUpSaga(
    private val userCommandService: UserCommandService
) : SimpleSaga<SignUpSaga.Data> {
    data class Data(
        val name: String,
        val password: String
    ) {
        var id: Long = 0
    }

    fun generateData(name: String, password: String) = Data(
        name = name,
        password = password
    )

    private val sagaDefinition = step()
        .invokeLocal(::createUser)
        .withCompensation(::createUserRollback)
        .build()

    override fun getSagaDefinition(): SagaDefinition<Data> = sagaDefinition

    fun createUser(data: Data) {
        data.id = userCommandService.createAggregate(
            name = data.name,
            password = data.password
        )
    }

    fun createUserRollback(data: Data) {
        userCommandService.deleteAggregateById(data.id)
    }
}
