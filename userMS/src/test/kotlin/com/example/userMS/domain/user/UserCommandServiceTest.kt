package com.example.userMS.domain.user

import com.example.shared.jwt.AuthTokenProvider
import com.example.userMS.common.enumerate.UserRoleTitleEnum
import com.example.userMS.domain.user.entity.User
import com.example.userMS.domain.user.entity.UserRole
import com.example.userMS.domain.user.repository.UserRepository
import com.example.userMS.domain.user.repository.UserRoleRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.time.LocalDateTime

@ExtendWith(MockitoExtension::class)
@DisplayNameGeneration(DisplayNameGenerator.Simple::class)
internal class UserCommandServiceTest {
    @InjectMocks
    private lateinit var userCommandService: UserCommandService

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var userRoleRepository: UserRoleRepository

    @Spy
    private lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Spy
    private lateinit var authTokenProvider: AuthTokenProvider

    @Nested
    inner class CreateOneTest {
        @Captor
        private lateinit var userCaptor: ArgumentCaptor<User>

        @Captor
        private lateinit var userRolesCaptor: ArgumentCaptor<List<UserRole>>

        private fun mockUserRepositorySave(userId: Long) {
            Mockito.`when`(userRepository.save(ArgumentMatchers.any()))
                .thenAnswer {
                    val newUser = it.arguments.getOrNull(0) as User
                    newUser.id = userId
                    newUser.createdAt = LocalDateTime.now()
                    newUser.updatedAt = LocalDateTime.now()

                    newUser
                }
        }

        @Test
        fun `when username, password is valid then save(all) method is called`() {
            // given
            val userId = 100L
            val name = "username"
            val password = "1234"

            mockUserRepositorySave(userId)

            // when
            userCommandService.createAggregate(name, password)
            Mockito.verify(userRepository).save(userCaptor.capture())
            Mockito.verify(userRoleRepository).saveAll(userRolesCaptor.capture())

            // then
            Assertions.assertThat(userCaptor.value.id).isEqualTo(userId)
            Assertions.assertThat(userCaptor.value.name).isEqualTo(name)
            Assertions.assertThat(userCaptor.value.password).isNotEqualTo(password)
            Assertions.assertThat(userRolesCaptor.value[0].title).isEqualTo(UserRoleTitleEnum.GENERAL)
            Assertions.assertThat(userRolesCaptor.value[0].userId).isEqualTo(userId)
        }
    }

    @Test
    fun deleteById() {
    }

    @Nested
    inner class LoginTest {
        private fun mockUserRepositoryFindOneByName(name: String, returned: User?) {
            Mockito.`when`(userRepository.findOneByName(name))
                .thenReturn(returned)
        }

        @Test
        fun `when username, password is valid then return jwt`() {
            // given
            val userId = 1L
            val name = "username"
            val password = "1234"
            val hashedPassword = bCryptPasswordEncoder.encode(password)
            val returned = User(
                id = userId,
                name = name,
                password = hashedPassword
            )
            returned.userRoles = mutableListOf(
                UserRole(id = 1L, title = UserRoleTitleEnum.GENERAL, userId = userId)
            )

            mockUserRepositoryFindOneByName(name, returned)

            // when
            val token = userCommandService.login(name, password)
            val decoded = authTokenProvider.decodeAccessToken(token)

            // then
            Assertions.assertThat(token).isNotNull
            Assertions.assertThat(decoded).isNotNull
        }

        @Test
        fun `when username is invalid then throw exception`() {
            // given
            val name = "username"
            val password = "1234"

            mockUserRepositoryFindOneByName(name, null)

            // when
            val result = Assertions.catchException {
                userCommandService.login(name, password)
            }

            // then
            Assertions.assertThat(result.message).isEqualTo("invalid name or password")
        }

        @Test
        fun `when password is invalid then throw exception`() {
            // given
            val userId = 1L
            val name = "username"
            val password = "1234"
            val hashedPassword = bCryptPasswordEncoder.encode(password)
            val returned = User(
                id = userId,
                name = name,
                password = hashedPassword
            )
            returned.userRoles = mutableListOf(
                UserRole(id = 1L, title = UserRoleTitleEnum.GENERAL, userId = userId)
            )

            mockUserRepositoryFindOneByName(name, returned)

            // when
            val invalidPassword = "4321"
            val result = Assertions.catchException {
                userCommandService.login(name, invalidPassword)
            }

            // then
            Assertions.assertThat(result.message).isEqualTo("invalid name or password")
        }
    }
}
