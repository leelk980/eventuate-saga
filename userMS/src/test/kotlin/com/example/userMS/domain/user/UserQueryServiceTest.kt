package com.example.userMS.domain.user

import com.example.userMS.domain.user.repository.UserRepository
import com.example.userMS.domain.user.repository.UserRoleRepository
import org.junit.jupiter.api.DisplayNameGeneration
import org.junit.jupiter.api.DisplayNameGenerator
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
@DisplayNameGeneration(DisplayNameGenerator.Simple::class)
internal class UserQueryServiceTest {
    @InjectMocks
    private lateinit var userQueryService: UserQueryService

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var userRoleRepository: UserRoleRepository
}
