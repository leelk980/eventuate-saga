package com.example.paymentMS.`interface`.handler

import com.example.shared.saga.event.OrderCreatedEvent
import io.eventuate.tram.events.subscriber.DomainEventEnvelope
import io.eventuate.tram.events.subscriber.DomainEventHandlers
import io.eventuate.tram.events.subscriber.DomainEventHandlersBuilder
import org.springframework.stereotype.Component

@Component
class DomainEventHandler {
    fun domainEventHandlersDefinitions(): DomainEventHandlers {
        return DomainEventHandlersBuilder
            .forAggregateType("OrderAggregate")
            .onEvent(OrderCreatedEvent::class.java, ::handleOrderCreated)
            .build()
    }

    private fun handleOrderCreated(event: DomainEventEnvelope<OrderCreatedEvent>) {
        println("handle order-created-event")
    }
}