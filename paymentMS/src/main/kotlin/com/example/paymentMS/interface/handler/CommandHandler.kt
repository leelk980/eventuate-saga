package com.example.paymentMS.`interface`.handler

import com.example.shared.saga.command.PayOrderCommand
import com.example.shared.saga.command.PayOrderRollbackCommand
import io.eventuate.tram.commands.consumer.CommandHandlerReplyBuilder
import io.eventuate.tram.commands.consumer.CommandHandlers
import io.eventuate.tram.commands.consumer.CommandMessage
import io.eventuate.tram.messaging.common.Message
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder
import org.springframework.stereotype.Component

@Component
class CommandHandler {
    fun commandHandlerDefinitions(): CommandHandlers {
        return SagaCommandHandlersBuilder
            .fromChannel("paymentMS")
            .onMessage(PayOrderCommand::class.java, ::handlePayOrder)
            .onMessage(PayOrderRollbackCommand::class.java, ::handlePayOrderRollback)
            .build()
    }

    private fun handlePayOrder(cm: CommandMessage<PayOrderCommand>): Message {
        println("결제 요청 후 완료")

        return CommandHandlerReplyBuilder
            .withSuccess(PayOrderCommand.Response(id = "12", success = true))
    }

    private fun handlePayOrderRollback(cm: CommandMessage<PayOrderRollbackCommand>): Message {
        println("결제 취소 완료")

        return CommandHandlerReplyBuilder
            .withSuccess(PayOrderRollbackCommand.Response(id = "12", success = true))
    }
}