package com.example.paymentMS.common.config

import com.example.paymentMS.`interface`.handler.CommandHandler
import com.example.paymentMS.`interface`.handler.DomainEventHandler
import io.eventuate.tram.events.subscriber.DomainEventDispatcher
import io.eventuate.tram.events.subscriber.DomainEventDispatcherFactory
import io.eventuate.tram.sagas.participant.SagaCommandDispatcher
import io.eventuate.tram.sagas.participant.SagaCommandDispatcherFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class EventuateConfig {
    private val msName = "paymentMS"

    @Bean
    fun commandHandlerDispatcher(
        target: CommandHandler,
        factory: SagaCommandDispatcherFactory
    ): SagaCommandDispatcher {
        val id = "${msName}-commandHandlerDispatcher"
        return factory.make(id, target.commandHandlerDefinitions())
    }

    @Bean
    fun domainEventHandlerDispatcher(
        target: DomainEventHandler,
        factory: DomainEventDispatcherFactory
    ): DomainEventDispatcher {
        val id = "${msName}-domainEventHandlerDispatcher"
        return factory.make(id, target.domainEventHandlersDefinitions())
    }
}