# Eventuate Tram Saga

## 실행전 준비 작업

- mysql
    - eventuate database에 eventuate.sql 실행
    - docker-compose에 mysql 접속정보 입력
- kafka cluster
    - docker-compose up -d

## 핵심 컨셉

- MSA + Spring Boot 데모 프로젝트
- hexagonal architecture + CQRS 패턴 일부분 차용
- Eventuate 기반의 오케스트레이션 Saga + 코레오그래피 Saga
    - 코드만 보면 오케스트레이션 기반의 동기적으로 통신하는 것 같아 보이지만,
    - outbox table + cdc + event-broker 사용해서 비동기 통신임..!!
    - @TODO: eventuate가 어디까지 해주며, 보상함수 잘 동작하는지 확인
- 스프링 시큐리티
- dgs 기반의 graphql api (예정..?)
- apollo federation (예정..?)

## 구현 사항

- userMS
    - 스프링 시큐리티 기반의 인증/인가 구현
        - @TODO: shared.security 모듈 고도화 필요
        - role 기반의 authorization 부분
    - mockito 활용해서 unit test 작성

- orderMS
    - 진입점 = orderController.createOrder()
    - orderMS와 paymentMS 사이의 트랜잭션을 구현
        - shared.saga 모듈의 command 기반으로 통신

- paymentMS
    - orderMS가 보내는 command handler 구현