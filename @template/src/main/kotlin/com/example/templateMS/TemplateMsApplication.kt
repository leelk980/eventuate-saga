package com.example.eventuate.saga

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TemlpateMSApplication

fun main(args: Array<String>) {
    runApplication<TemlpateMSApplication>(*args)
}
