package com.example.orderMS.`interface`.internal

import com.example.orderMS.application.FacadeService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v2")
class OrderInternalController(
    private val orderFacadeService: FacadeService
) {
    @GetMapping("/orders/users/{userId}")
    fun findAllOrdersByUserId(@PathVariable userId: Long): String {
        orderFacadeService.findOne()
        return "test $userId"
    }
}
