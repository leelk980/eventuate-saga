package com.example.orderMS.`interface`.external

import com.example.orderMS.application.SagaService
import com.example.orderMS.`interface`.external.req.CreateOrderBody
import com.example.shared.security.annotation.User
import com.example.shared.security.component.CurrUser
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1")
class OrderController(
    private val sagaService: SagaService
) {
    @PostMapping("/orders")
    fun createOrder(@User user: CurrUser, @RequestBody body: CreateOrderBody): Long {
        return sagaService.createOrder(user.id, body)
    }
}
