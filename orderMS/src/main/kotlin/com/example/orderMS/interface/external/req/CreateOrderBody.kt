package com.example.orderMS.`interface`.external.req

data class CreateOrderBody(
    val price: Long,
    val address: String,
)