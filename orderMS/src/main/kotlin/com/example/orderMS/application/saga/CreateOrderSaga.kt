package com.example.orderMS.application.saga

import com.example.orderMS.common.enum.OrderStatusEnum
import com.example.orderMS.domain.order.OrderCommandService
import com.example.shared.saga.command.PayOrderCommand
import com.example.shared.saga.command.PayOrderRollbackCommand
import com.example.shared.saga.event.OrderCreatedEvent
import io.eventuate.tram.commands.consumer.CommandWithDestination
import io.eventuate.tram.commands.consumer.CommandWithDestinationBuilder
import io.eventuate.tram.events.publisher.DomainEventPublisher
import io.eventuate.tram.sagas.orchestration.SagaDefinition
import io.eventuate.tram.sagas.simpledsl.SimpleSaga
import org.springframework.stereotype.Component

@Component
class CreateOrderSaga(
    val orderCommandService: OrderCommandService,
    val domainEventPublisher: DomainEventPublisher
) : SimpleSaga<CreateOrderSaga.Data> {
    data class Data(
        var id: Long = 0,
        var price: Long,
        var address: String,
        var userId: Long,
        var paymentUuid: String?
    )

    fun generateData(price: Long, address: String, userId: Long) = Data(
        price = price,
        address = address,
        userId = userId,
        paymentUuid = null
    )

    private val sagaDefinition = step()
        .invokeLocal(::createOrder)
        .withCompensation(::createOrderRollback)
        .step()
        .invokeParticipant(::payOrderCommand)
        .onReply(PayOrderCommand.Response::class.java, ::payOrderCommandReply)
        .withCompensation(::payOrderRollbackCommand)
        .step()
        .invokeLocal(::finishOrder)
        .withCompensation(::finishOrderRollback)
        .step()
        .invokeLocal(::publishDomainEvent)
        .build()

    override fun getSagaDefinition(): SagaDefinition<Data> = sagaDefinition

    fun createOrder(data: Data) {
        data.id = orderCommandService.createAggregate(
            data.price,
            data.address,
            data.userId
        )
    }

    fun createOrderRollback(data: Data) {
        orderCommandService.deleteAggregateById(data.id)
    }

    fun payOrderCommand(data: Data): CommandWithDestination = CommandWithDestinationBuilder
        .send(PayOrderCommand(orderId = data.id, price = data.price))
        .to("paymentMS")
        .build()

    fun payOrderRollbackCommand(data: Data): CommandWithDestination = CommandWithDestinationBuilder
        .send(PayOrderRollbackCommand(orderId = data.id, price = data.price))
        .to("paymentMS")
        .build()

    fun payOrderCommandReply(data: Data, response: PayOrderCommand.Response) {
        println("reply")
        data.paymentUuid = response.id
    }

    fun finishOrder(data: Data) {
        orderCommandService.updateAggregateById(
            id = data.id,
            status = OrderStatusEnum.COMPLETE,
            paymentUuid = data.paymentUuid
        )
    }

    fun finishOrderRollback(data: Data) {
        orderCommandService.updateAggregateById(
            id = data.id,
            status = OrderStatusEnum.PENDING,
            paymentUuid = null
        )
    }

    fun publishDomainEvent(data: Data) {
        val aggregateType = "OrderAggregate"
        val event = OrderCreatedEvent(id = data.id)

        domainEventPublisher.publish(
            aggregateType,
            data.id,
            listOf(event)
        )
    }
}
