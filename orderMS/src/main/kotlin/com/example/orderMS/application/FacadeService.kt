package com.example.orderMS.application

import com.example.orderMS.domain.order.OrderQueryService
import org.springframework.stereotype.Service

@Service
class FacadeService(
    private val orderQueryService: OrderQueryService
) {
    fun findOne() {}
}
