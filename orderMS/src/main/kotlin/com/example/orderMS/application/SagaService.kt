package com.example.orderMS.application

import com.example.orderMS.application.saga.CreateOrderSaga
import com.example.orderMS.`interface`.external.req.CreateOrderBody
import io.eventuate.tram.sagas.orchestration.SagaInstanceFactory
import org.springframework.stereotype.Service

@Service
class SagaService(
    private val sagaInstanceFactory: SagaInstanceFactory,
    private val createOrderSaga: CreateOrderSaga
) {
    fun createOrder(userId: Long, body: CreateOrderBody): Long {
        val data = createOrderSaga.generateData(
            price = body.price,
            address = body.address,
            userId = userId,
        )
        sagaInstanceFactory.create(createOrderSaga, data)

        return data.id
    }
}