package com.example.orderMS

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OrderMsApplication

fun main(args: Array<String>) {
    runApplication<OrderMsApplication>(*args)
}
