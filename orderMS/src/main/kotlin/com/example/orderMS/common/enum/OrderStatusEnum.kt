package com.example.orderMS.common.enum

enum class OrderStatusEnum(val value: String) {
    PENDING("결제대기"),
    COMPLETE("결제완료"),
}