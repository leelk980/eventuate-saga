package com.example.orderMS.common.config

import com.example.shared.security.config.BaseSecurityConfig
import org.springframework.context.annotation.Configuration

@Configuration
class SecurityConfig : BaseSecurityConfig()