package com.example.orderMS.domain.order.repository

import com.example.orderMS.domain.order.entity.OrderEntity
import org.springframework.data.jpa.repository.JpaRepository

interface OrderRepository : JpaRepository<OrderEntity, Long>