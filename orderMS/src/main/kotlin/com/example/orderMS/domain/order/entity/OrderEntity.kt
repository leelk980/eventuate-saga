package com.example.orderMS.domain.order.entity

import com.example.orderMS.common.enum.OrderStatusEnum
import javax.persistence.*

@Entity
@Table(name = "orders")
open class OrderEntity(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0,

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    open var status: OrderStatusEnum,

    @Column(name = "price", nullable = false)
    open var price: Long,

    @Column(name = "address", nullable = false)
    open var address: String,

    @Column(name = "user_id", nullable = false)
    open var userId: Long,

    @Column(name = "paymentUuid", nullable = true)
    open var paymentUuid: String?,
)