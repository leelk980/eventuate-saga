package com.example.orderMS.domain.order

import com.example.orderMS.domain.order.entity.OrderEntity
import com.example.orderMS.domain.order.repository.OrderRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class OrderQueryService(
    private val orderRepository: OrderRepository
) {
    fun findOneById(id: Long): Optional<OrderEntity> {
        return orderRepository.findById(id)
    }
}
