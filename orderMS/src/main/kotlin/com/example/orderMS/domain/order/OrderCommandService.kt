package com.example.orderMS.domain.order

import com.example.orderMS.common.enum.OrderStatusEnum
import com.example.orderMS.domain.order.entity.OrderEntity
import com.example.orderMS.domain.order.repository.OrderRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class OrderCommandService(
    private val orderRepository: OrderRepository
) {
    fun createAggregate(price: Long, address: String, userId: Long): Long {
        val newOrder = orderRepository.save(
            OrderEntity(
                status = OrderStatusEnum.PENDING,
                price = price,
                address = address,
                userId = userId,
                paymentUuid = null
            )
        )

        return newOrder.id
    }

    fun updateAggregateById(id: Long, status: OrderStatusEnum?, paymentUuid: String?): Long {
        val order = orderRepository.findByIdOrNull(id) ?: throw Exception("Not found")

        order.status = status ?: order.status
        order.paymentUuid = paymentUuid
        return orderRepository.save(order).id
    }

    fun deleteAggregateById(id: Long): Long {
        orderRepository.deleteById(id)
        return id
    }
}
