rootProject.name = "eventuate-saga"

// shared modules
include("@shared:saga")
include("@shared:security")
include("@shared:jwt")
include("@shared:internalClient")
// applications
include("userMS")
include("orderMS")
include("paymentMS")
